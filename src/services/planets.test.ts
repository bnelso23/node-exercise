import { PlanetsResponse, Planet } from "../types"
import { getPlanetsFromSWAPI } from "./planets"
import axios from "axios"

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>

const page1 : PlanetsResponse = {
    data: {
            "count": 60,
            "next": "http://swapi.dev/api/planets/?page=2",
            "previous": null,
            "results": [
                {
                    "name": "Tatooine",
                    "rotation_period": "23",
                    "orbital_period": "304",
                    "diameter": "10465",
                    "climate": "arid",
                    "gravity": "1 standard",
                    "terrain": "desert",
                    "surface_water": "1",
                    "population": "200000",
                    "residents": [
                        "http://swapi.dev/api/people/1/",
                        "http://swapi.dev/api/people/2/",
                        "http://swapi.dev/api/people/4/",
                        "http://swapi.dev/api/people/6/",
                        "http://swapi.dev/api/people/7/",
                        "http://swapi.dev/api/people/8/",
                        "http://swapi.dev/api/people/9/",
                        "http://swapi.dev/api/people/11/",
                        "http://swapi.dev/api/people/43/",
                        "http://swapi.dev/api/people/62/"
                    ],
                    "films": [
                        "http://swapi.dev/api/films/1/",
                        "http://swapi.dev/api/films/3/",
                        "http://swapi.dev/api/films/4/",
                        "http://swapi.dev/api/films/5/",
                        "http://swapi.dev/api/films/6/"
                    ],
                    "created": "2014-12-09T13:50:49.641000Z",
                    "edited": "2014-12-20T20:58:18.411000Z",
                    "url": "http://swapi.dev/api/planets/1/"
                },
                {
                    "name": "Alderaan",
                    "rotation_period": "24",
                    "orbital_period": "364",
                    "diameter": "12500",
                    "climate": "temperate",
                    "gravity": "1 standard",
                    "terrain": "grasslands, mountains",
                    "surface_water": "40",
                    "population": "2000000000",
                    "residents": [
                        "http://swapi.dev/api/people/5/",
                        "http://swapi.dev/api/people/68/",
                        "http://swapi.dev/api/people/81/"
                    ],
                    "films": [
                        "http://swapi.dev/api/films/1/",
                        "http://swapi.dev/api/films/6/"
                    ],
                    "created": "2014-12-10T11:35:48.479000Z",
                    "edited": "2014-12-20T20:58:18.420000Z",
                    "url": "http://swapi.dev/api/planets/2/"
                }
            ]
        }
    }

    const page2 : PlanetsResponse = {
        data: {
                "count": 60,
                "next": null,
                "previous": "http://swapi.dev/api/planets",
                "results": [
                    {
                        "name": "Geonosis",
                        "rotation_period": "30",
                        "orbital_period": "256",
                        "diameter": "11370",
                        "climate": "temperate, arid",
                        "gravity": "0.9 standard",
                        "terrain": "rock, desert, mountain, barren",
                        "surface_water": "5",
                        "population": "100000000000",
                        "residents": [
                            "http://swapi.dev/api/people/63/"
                        ],
                        "films": [
                            "http://swapi.dev/api/films/5/"
                        ],
                        "created": "2014-12-10T12:47:22.350000Z",
                        "edited": "2014-12-20T20:58:18.437000Z",
                        "url": "http://swapi.dev/api/planets/11/"
                    },
                    {
                        "name": "Utapau",
                        "rotation_period": "27",
                        "orbital_period": "351",
                        "diameter": "12900",
                        "climate": "temperate, arid, windy",
                        "gravity": "1 standard",
                        "terrain": "scrublands, savanna, canyons, sinkholes",
                        "surface_water": "0.9",
                        "population": "95000000",
                        "residents": [
                            "http://swapi.dev/api/people/83/"
                        ],
                        "films": [
                            "http://swapi.dev/api/films/6/"
                        ],
                        "created": "2014-12-10T12:49:01.491000Z",
                        "edited": "2014-12-20T20:58:18.439000Z",
                        "url": "http://swapi.dev/api/planets/12/"
                    }
                ]
            }
        }

const expectedResult : Planet[] = [
    {
        "name": "Tatooine",
        "rotation_period": "23",
        "orbital_period": "304",
        "diameter": "10465",
        "climate": "arid",
        "gravity": "1 standard",
        "terrain": "desert",
        "surface_water": "1",
        "population": "200000",
        "residents": [
            "http://swapi.dev/api/people/1/",
            "http://swapi.dev/api/people/2/",
            "http://swapi.dev/api/people/4/",
            "http://swapi.dev/api/people/6/",
            "http://swapi.dev/api/people/7/",
            "http://swapi.dev/api/people/8/",
            "http://swapi.dev/api/people/9/",
            "http://swapi.dev/api/people/11/",
            "http://swapi.dev/api/people/43/",
            "http://swapi.dev/api/people/62/"
        ],
        "films": [
            "http://swapi.dev/api/films/1/",
            "http://swapi.dev/api/films/3/",
            "http://swapi.dev/api/films/4/",
            "http://swapi.dev/api/films/5/",
            "http://swapi.dev/api/films/6/"
        ],
        "created": "2014-12-09T13:50:49.641000Z",
        "edited": "2014-12-20T20:58:18.411000Z",
        "url": "http://swapi.dev/api/planets/1/"
    },
    {
        "name": "Alderaan",
        "rotation_period": "24",
        "orbital_period": "364",
        "diameter": "12500",
        "climate": "temperate",
        "gravity": "1 standard",
        "terrain": "grasslands, mountains",
        "surface_water": "40",
        "population": "2000000000",
        "residents": [
            "http://swapi.dev/api/people/5/",
            "http://swapi.dev/api/people/68/",
            "http://swapi.dev/api/people/81/"
        ],
        "films": [
            "http://swapi.dev/api/films/1/",
            "http://swapi.dev/api/films/6/"
        ],
        "created": "2014-12-10T11:35:48.479000Z",
        "edited": "2014-12-20T20:58:18.420000Z",
        "url": "http://swapi.dev/api/planets/2/"
    },
    {
        "name": "Geonosis",
        "rotation_period": "30",
        "orbital_period": "256",
        "diameter": "11370",
        "climate": "temperate, arid",
        "gravity": "0.9 standard",
        "terrain": "rock, desert, mountain, barren",
        "surface_water": "5",
        "population": "100000000000",
        "residents": [
            "http://swapi.dev/api/people/63/"
        ],
        "films": [
            "http://swapi.dev/api/films/5/"
        ],
        "created": "2014-12-10T12:47:22.350000Z",
        "edited": "2014-12-20T20:58:18.437000Z",
        "url": "http://swapi.dev/api/planets/11/"
    },
    {
        "name": "Utapau",
        "rotation_period": "27",
        "orbital_period": "351",
        "diameter": "12900",
        "climate": "temperate, arid, windy",
        "gravity": "1 standard",
        "terrain": "scrublands, savanna, canyons, sinkholes",
        "surface_water": "0.9",
        "population": "95000000",
        "residents": [
            "http://swapi.dev/api/people/83/"
        ],
        "films": [
            "http://swapi.dev/api/films/6/"
        ],
        "created": "2014-12-10T12:49:01.491000Z",
        "edited": "2014-12-20T20:58:18.439000Z",
        "url": "http://swapi.dev/api/planets/12/"
    }
]


test("Should return all the planets", async () => {
    mockedAxios.get.mockResolvedValueOnce(page1);
    mockedAxios.get.mockResolvedValueOnce(page2);

    const planets = await getPlanetsFromSWAPI();

    expect(planets).toEqual(expectedResult)
})