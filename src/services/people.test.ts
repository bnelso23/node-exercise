import axios from 'axios';
import { PeopleResponse, Person } from '../types/index';
import {getPeopleFromSWAPI} from './people'

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>

const page8 : PeopleResponse = {
    data:{
        "count": 82,
        "next": "http://swapi.dev/api/people/?page=9",
        "previous": "http://swapi.dev/api/people/?page=7",
        "results": [
            {
                "name": "Lama Su",
                "height": "229",
                "mass": "88",
                "hair_color": "none",
                "skin_color": "grey",
                "eye_color": "black",
                "birth_year": "unknown",
                "gender": "male",
                "homeworld": "http://swapi.dev/api/planets/10/",
                "films": [
                    "http://swapi.dev/api/films/5/"
                ],
                "species": [
                    "http://swapi.dev/api/species/32/"
                ],
                "vehicles": [],
                "starships": [],
                "created": "2014-12-20T17:30:50.416000Z",
                "edited": "2014-12-20T21:17:50.473000Z",
                "url": "http://swapi.dev/api/people/72/"
            },
            {
                "name": "Taun We",
                "height": "213",
                "mass": "unknown",
                "hair_color": "none",
                "skin_color": "grey",
                "eye_color": "black",
                "birth_year": "unknown",
                "gender": "female",
                "homeworld": "http://swapi.dev/api/planets/10/",
                "films": [
                    "http://swapi.dev/api/films/5/"
                ],
                "species": [
                    "http://swapi.dev/api/species/32/"
                ],
                "vehicles": [],
                "starships": [],
                "created": "2014-12-20T17:31:21.195000Z",
                "edited": "2014-12-20T21:17:50.474000Z",
                "url": "http://swapi.dev/api/people/73/"
            }
        ]
    }
    }


const page9: PeopleResponse  = {
    data:{
        "count": 82,
        "next": null,
        "previous": "http://swapi.dev/api/people/?page=8",
        "results": [
            {
                "name": "Sly Moore",
                "height": "178",
                "mass": "48",
                "hair_color": "none",
                "skin_color": "pale",
                "eye_color": "white",
                "birth_year": "unknown",
                "gender": "female",
                "homeworld": "http://swapi.dev/api/planets/60/",
                "films": [
                    "http://swapi.dev/api/films/5/",
                    "http://swapi.dev/api/films/6/"
                ],
                "species": [],
                "vehicles": [],
                "starships": [],
                "created": "2014-12-20T20:18:37.619000Z",
                "edited": "2014-12-20T21:17:50.496000Z",
                "url": "http://swapi.dev/api/people/82/"
            },
            {
                "name": "Tion Medon",
                "height": "206",
                "mass": "80",
                "hair_color": "none",
                "skin_color": "grey",
                "eye_color": "black",
                "birth_year": "unknown",
                "gender": "male",
                "homeworld": "http://swapi.dev/api/planets/12/",
                "films": [
                    "http://swapi.dev/api/films/6/"
                ],
                "species": [
                    "http://swapi.dev/api/species/37/"
                ],
                "vehicles": [],
                "starships": [],
                "created": "2014-12-20T20:35:04.260000Z",
                "edited": "2014-12-20T21:17:50.498000Z",
                "url": "http://swapi.dev/api/people/83/"
            }
        ]
    }
    }


test('getPeopleFromSWAPI should return all people', async () => {
    mockedAxios.get.mockResolvedValueOnce(page8);
    mockedAxios.get.mockResolvedValueOnce(page9);

    const people = await getPeopleFromSWAPI()

    const expectedResult: Person[] = [
        {
            "name": "Lama Su",
            "height": "229",
            "mass": "88",
            "hair_color": "none",
            "skin_color": "grey",
            "eye_color": "black",
            "birth_year": "unknown",
            "gender": "male",
            "homeworld": "http://swapi.dev/api/planets/10/",
            "films": [
                "http://swapi.dev/api/films/5/"
            ],
            "species": [
                "http://swapi.dev/api/species/32/"
            ],
            "vehicles": [],
            "starships": [],
            "created": "2014-12-20T17:30:50.416000Z",
            "edited": "2014-12-20T21:17:50.473000Z",
            "url": "http://swapi.dev/api/people/72/"
        },
        {
            "name": "Taun We",
            "height": "213",
            "mass": "unknown",
            "hair_color": "none",
            "skin_color": "grey",
            "eye_color": "black",
            "birth_year": "unknown",
            "gender": "female",
            "homeworld": "http://swapi.dev/api/planets/10/",
            "films": [
                "http://swapi.dev/api/films/5/"
            ],
            "species": [
                "http://swapi.dev/api/species/32/"
            ],
            "vehicles": [],
            "starships": [],
            "created": "2014-12-20T17:31:21.195000Z",
            "edited": "2014-12-20T21:17:50.474000Z",
            "url": "http://swapi.dev/api/people/73/"
        },
        {
            "name": "Sly Moore",
            "height": "178",
            "mass": "48",
            "hair_color": "none",
            "skin_color": "pale",
            "eye_color": "white",
            "birth_year": "unknown",
            "gender": "female",
            "homeworld": "http://swapi.dev/api/planets/60/",
            "films": [
                "http://swapi.dev/api/films/5/",
                "http://swapi.dev/api/films/6/"
            ],
            "species": [],
            "vehicles": [],
            "starships": [],
            "created": "2014-12-20T20:18:37.619000Z",
            "edited": "2014-12-20T21:17:50.496000Z",
            "url": "http://swapi.dev/api/people/82/"
        },
        {
            "name": "Tion Medon",
            "height": "206",
            "mass": "80",
            "hair_color": "none",
            "skin_color": "grey",
            "eye_color": "black",
            "birth_year": "unknown",
            "gender": "male",
            "homeworld": "http://swapi.dev/api/planets/12/",
            "films": [
                "http://swapi.dev/api/films/6/"
            ],
            "species": [
                "http://swapi.dev/api/species/37/"
            ],
            "vehicles": [],
            "starships": [],
            "created": "2014-12-20T20:35:04.260000Z",
            "edited": "2014-12-20T21:17:50.498000Z",
            "url": "http://swapi.dev/api/people/83/"
        }

    ]

    expect(people).toEqual(expectedResult)

})