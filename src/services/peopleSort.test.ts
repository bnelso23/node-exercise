import {peopleSort} from './peopleSort'


test("SortBy=height/mass should be able to handle commas", ()=>{
    const array = [
        {height: "1,200"},
        {height: "1000000"},
        {height: "100"},
        {height: "2000"},
        {height: "1,385"}
    ]

    const sortedArray = [
        {height: "100"},
        {height: "1,200"},
        {height: "1,385"},
        {height: "2000"},
        {height: "1000000"}
    ]

    expect(peopleSort(array,"height")).toEqual(sortedArray)
})

test("SortBy=height/mass should be able to handle unknown", ()=>{
    const array = [
        {height: "unknown"},
        {height: "1000000"},
        {height: "unknown"},
        {height: "unknown"},
        {height: "1,385"}
    ]

    const sortedArray = [
        {height: "1,385"},
        {height: "1000000"},
        {height: "unknown"},
        {height: "unknown"},
        {height: "unknown"}
    ]

    expect(peopleSort(array,"height")).toEqual(sortedArray)
})

test("SortBy=name should return array with names in alphabetical order",()=>{
    const array = [
        {name:"Landon"},
        {name:"Adam"},
        {name: "Ben"},
        {name: "Jim-jam-joe"},
        {name: "C3-P0"},
        {name: "R2-D2"}
    ]

    const sortedArray = [
        {name:"Adam"},
        {name: "Ben"},
        {name: "C3-P0"},
        {name: "Jim-jam-joe"},
        {name:"Landon"},
        {name: "R2-D2"}
    ]

    expect(peopleSort(array, "name")).toEqual(sortedArray)
})