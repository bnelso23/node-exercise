import axios from "axios";
import { PeopleResponseData, Person } from "../types";
import {peopleSort} from "./peopleSort"

export const getPeopleFromSWAPI = async (sortBy:any = null) =>{
    try{
        let people = Array<Person>();
        let data : PeopleResponseData | null = null;
        let next = 'https://swapi.dev/api/people'
        do{
            const res = await axios.get(next);
            data = res.data;
            people = people.concat(data.results)
            next = data.next
            console.log(next)
        }while(next)

        if(sortBy){
            return peopleSort(people, sortBy);
        }else{
            return people
        }

    }catch(err){
        console.log('Got an error in getPeopleFromSWAPI')
        throw err
    }

}