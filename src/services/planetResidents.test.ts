import { Person, Planet } from "../types"
import { getPlanetResidents } from "./planetResidents"


test("should replace resident urls in planets with peoples names", () => {

    const people:Person[] = [
        {
            "name": "Finis Valorum",
            "height": "170",
            "mass": "unknown",
            "hair_color": "blond",
            "skin_color": "fair",
            "eye_color": "blue",
            "birth_year": "91BBY",
            "gender": "male",
            "homeworld": "http://swapi.dev/api/planets/9/",
            "films": [
                "http://swapi.dev/api/films/4/"
            ],
            "species": [],
            "vehicles": [],
            "starships": [],
            "created": "2014-12-19T17:21:45.915000Z",
            "edited": "2014-12-20T21:17:50.379000Z",
            "url": "http://swapi.dev/api/people/34/"
        },
        {
            "name": "Adi Gallia",
            "height": "184",
            "mass": "50",
            "hair_color": "none",
            "skin_color": "dark",
            "eye_color": "blue",
            "birth_year": "unknown",
            "gender": "female",
            "homeworld": "http://swapi.dev/api/planets/9/",
            "films": [
                "http://swapi.dev/api/films/4/",
                "http://swapi.dev/api/films/6/"
            ],
            "species": [
                "http://swapi.dev/api/species/23/"
            ],
            "vehicles": [],
            "starships": [],
            "created": "2014-12-20T10:29:11.661000Z",
            "edited": "2014-12-20T21:17:50.432000Z",
            "url": "http://swapi.dev/api/people/55/"
        },
        {
            "name": "Jocasta Nu",
            "height": "167",
            "mass": "unknown",
            "hair_color": "white",
            "skin_color": "fair",
            "eye_color": "blue",
            "birth_year": "unknown",
            "gender": "female",
            "homeworld": "http://swapi.dev/api/planets/9/",
            "films": [
                "http://swapi.dev/api/films/5/"
            ],
            "species": [
                "http://swapi.dev/api/species/1/"
            ],
            "vehicles": [],
            "starships": [],
            "created": "2014-12-20T17:32:51.996000Z",
            "edited": "2014-12-20T21:17:50.476000Z",
            "url": "http://swapi.dev/api/people/74/"
        }


    ]

    const planets: Planet[] =[
        {
            "name": "Coruscant",
            "rotation_period": "24",
            "orbital_period": "368",
            "diameter": "12240",
            "climate": "temperate",
            "gravity": "1 standard",
            "terrain": "cityscape, mountains",
            "surface_water": "unknown",
            "population": "1000000000000",
            "residents": [
                "http://swapi.dev/api/people/34/",
                "http://swapi.dev/api/people/55/",
                "http://swapi.dev/api/people/74/"
            ],
            "films": [
                "http://swapi.dev/api/films/3/",
                "http://swapi.dev/api/films/4/",
                "http://swapi.dev/api/films/5/",
                "http://swapi.dev/api/films/6/"
            ],
            "created": "2014-12-10T11:54:13.921000Z",
            "edited": "2014-12-20T20:58:18.432000Z",
            "url": "http://swapi.dev/api/planets/9/"
        },
    ]

    const expectedData: Planet[] = [
        {
            "name": "Coruscant",
            "rotation_period": "24",
            "orbital_period": "368",
            "diameter": "12240",
            "climate": "temperate",
            "gravity": "1 standard",
            "terrain": "cityscape, mountains",
            "surface_water": "unknown",
            "population": "1000000000000",
            "residents": [
                "Finis Valorum",
                "Adi Gallia",
                "Jocasta Nu"
            ],
            "films": [
                "http://swapi.dev/api/films/3/",
                "http://swapi.dev/api/films/4/",
                "http://swapi.dev/api/films/5/",
                "http://swapi.dev/api/films/6/"
            ],
            "created": "2014-12-10T11:54:13.921000Z",
            "edited": "2014-12-20T20:58:18.432000Z",
            "url": "http://swapi.dev/api/planets/9/"
        },
    ]

    expect(getPlanetResidents(planets,people)).toEqual(expectedData)


})