import axios from "axios";
import { Planet, PlanetsResponseData } from "../types";

export const getPlanetsFromSWAPI = async () =>{
    try{
        let planets = Array<Planet>();
        let data : PlanetsResponseData | null = null;
        let next = 'https://swapi.dev/api/planets'
        do{
            const res = await axios.get(next);
            data = res.data;
            planets = planets.concat(data.results)
            next = data.next
            console.log(next)
        }while(next)

        return planets

    }catch(err){
        console.log('Got an error in getPlanetsFromSWAPI')
        throw err
    }

}