export const peopleSort = (arrayToSort: any[], sortBy:string) => {
    if(sortBy === 'height' || sortBy === 'mass'){
        return arrayToSort.sort((a,b) => {
            return(a[sortBy] ==="unknown" ? Infinity : Number(a[sortBy].replace(",","")) > (b[sortBy] === "unknown" ? Infinity :Number(b[sortBy].replace(",",""))) ? 1: -1
        )})
    }
    return arrayToSort.sort((a,b) => (a[sortBy] > b[sortBy]) ? 1: -1)
}