import { Person, Planet } from "../types"

export const getPlanetResidents = (planets:Planet[], people:Person[]) => {
    planets.forEach(planet => {
       planet.residents = planet.residents.map(personUrl =>{
            const foundPerson = people.find(person => person.url === personUrl)
            return foundPerson.name
        })
    })

    return planets
}