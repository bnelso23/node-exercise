import express from "express"
import { getPeopleFromSWAPI } from './services/people'
import { getPlanetResidents } from "./services/planetResidents";
import { getPlanetsFromSWAPI } from "./services/planets";

const app = express();
const port = process.env.PORT || 8080

app.get('/people', async (req, res) => {
    try{
        const sortBy = req.query.sortBy;
        const people = await getPeopleFromSWAPI(sortBy);
        res.send(people)
    }catch(err){
        res.status(500)
        res.send({mesage:err.message})
    }
})

app.get('/planets', async (req, res) => {
    try{
        const [people, planets] = await Promise.all([getPeopleFromSWAPI(),getPlanetsFromSWAPI()])
        const planetsWithResidentNames = getPlanetResidents(planets,people);
        res.send(planetsWithResidentNames)
    }catch(err){
        res.status(500)
        res.send({mesage:err.message})
    }
})

app.listen(port)